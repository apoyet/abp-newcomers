# Useful links

In this section, we collect a series of useful links you might need during your stay at CERN. 

### The Welcome Brochure

CERN provides a nice brochure with all information you need when you arrive at CERN. It is available [here](https://cds.cern.ch/record/1995620/files/Welcome%20Brochure.pdf?version=1){target=_blank}

### The CERN Staff Association

Every member of the CERN personnel can join the Staff Association. Its goal is to defend the members of personnel, but also to provide some useful services. Access their [website](http://staff-association.web.cern.ch/){target=_blank}.

The Staff Association also provides a large variety of clubs that might interest you. Click [here](http://staff-association.web.cern.ch/clubs){target=_blank} to learn more. 

### The CERN Welcome Club

You want to learn a new language? Or participate to a yoga class? Click [here](https://club-welcome.web.cern.ch/){target=_blank} to discover all the activities proposed by the CERN Welcome Club. 

### The BE Newsletter

All the news about the Beam Departement (BE) is available in the bimensual newsletters. You can find them by clicking [here](https://beams.web.cern.ch/aggregator/sources/2){target=_blank}.

### Health at CERN

Being member of CERN personnel, you are covered by the health insurance scheme provided by UNIQA. Useful information concerning this scheme can be found [here](https://hr-dep.web.cern.ch/chis/cern-health-insurance-scheme){target=_blank}. Typically, after having been to the doctor, or after buying medicines, you can ask for the reimbursement by filling this [form](https://cds.cern.ch/record/1999140/files/CHIS_F01%20-%20Claim%20for%20the%20reimbursement%20of%20medical%20expenses.pdf){target=_blank} and sending it back to UNIQA using their specific blue envelop directly through internal mail. 

CERN cares about your health and the way you feel at work. In that respect, the program Work Well Feel Well (WWFW) has been created. Learn more [here](https://hr-dep.web.cern.ch/WWFW){target=_blank}.

### CERN and social media

CERN Communication Team is doing a great job to share what is CERN and what we are doing on a lot of different social media. You can therefore find CERN and the experiments on Facebook, Twitter, Instagram, etc... All the links are summarized [here](https://communications.web.cern.ch/social-media){target=_blank}.