# Useful Codes

On this page, you will find the most used codes among our groups, with links to both simple examples/tutorials and documentations


### Methodical Accelerator Design (MAD-X)

This code is the reference code used at CERN for particle accelerator design and simulations. You can find all the information you need, releases and documentation by clicking [here](http://madx.web.cern.ch/madx/){target=_blank}.