# Welcome at CERN and in the ABP group!

This website is here to help you, as newcomer, to get more familiar with the usual tools used in our group. The instructions given in this site are very simple and if you need to further, please refer to the 'To Go Further' section, where you will find links to other more detailed websites. 

Contact: axel.poyet@cern.ch.

Thanks!